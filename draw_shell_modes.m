% -----------------------------------------------------------------
function draw_shell_modes(h_fig,n_max,l_max,ff_shell,V_P,full_name)
% -----------------------------------------------------------------
% 190625 HCN copied from draw_elastic_modes
% function to draw the frequencies of the modes of a spherical elastic shell
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
figure(h_fig)
hold on
%
title1 = '_nS_l mode frequencies of elastic spherical shell';
title([title1 char(10) full_name ' - ' date]);
xlabel('mode angular number l')
ylabel(['shell mode frequency (Hz) (V_P = ' num2str(V_P) ' m/s)'])
box on
grid on
%
for nn=n_max:-1:0
	nnn=nn+1;
	to_legend = int2str(nn);
	if (nn==n_max)
	   to_legend = ['n=' to_legend];
	end
	plot((1:l_max+1)-1,ff_shell(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor', coul(nnn), 'DisplayName',to_legend)
end
%
legend('show')
