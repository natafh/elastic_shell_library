% -------------------------------------------------------------------------------
function [delta_f_ratio, ill_A, A_rcond] = approximate_shell_deltaf(ll,ka,nu,bar, c_ratio,rho_ratio)
% -------------------------------------------------------------------------------
% returns the frequency correction defined by Mehl (1985) equation (26)
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   ka : k*a of rigid shell mode (z_ns in Mehl) 
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%   nu : Poisson coefficient of the shell
%   c_ratio : V_P/csound velocity ratio
%   rho_ratio : shell density/gas density
%
% outputs
%   delta_f_ratio : delta_f/f correction for acoustic mode of the rigid shell with ka=kk(nn,ll) 
%   ill_A : true if matrix A is ill-conditioned
%   A_rcond : rcond of the A matrix
%
kla = ka./c_ratio; % k_l*a in the shell
[S_n, ill_A, A_rcond] = S_n_admittance_factor(ll,kla,nu,bar,c_ratio);
%
delta_f_ratio = S_n./(1 -ll*(ll+1)./ka^2)./(rho_ratio*c_ratio^2);
