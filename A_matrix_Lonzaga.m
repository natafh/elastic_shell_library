% ------------------------------------------------------------
function A = A_matrix_Lonzaga(ll,ka,nu,bar,c_ratio,rho_ratio)
% ------------------------------------------------------------
% returns the matrix A defined by Lonzaga et al (2011) in their Appendix, controlling
% modes in a fluid-filled spherical elastic shell extending from r=a to r=b
% with Poisson coefficient nu, assuming free boundary condition at r=b (no radiation).
% Resonance occurs when det(A) = 0.
% Subscripts _l and _t stand for longitudinal (P) and transversal (S), respectively
%
% 190708 HCN 
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   ka : k*a variable
%   nu : Poisson coefficient of the shell
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%   c_ratio : V_P/csound velocity ratio
%   rho_ratio : shell density/gas density
%
% outputs
%   A : A (5x5) matrix
%
clt = sqrt(2*(1-nu)./(1-2*nu)); % V_P/V_S (or k_S/k_P) ratio in the shell
clt2 = 2*(1-nu)./(1-2*nu); % clt^2 ratio
kla = ka./c_ratio; % k_l*a in the shell
klb = bar*kla; % k_l*b
kta = clt*kla; % k_t*a
ktb = bar*kta; % k_t*b
%
A = zeros(5,5); % A is a 5x5 matrix
% ----------------
A(1,1) = (2*ll*(ll-1)/kla^2 - clt^2)*kla*spherical_bessel_j(ll,kla) + 4*spherical_bessel_j(ll+1,kla);
A(2,1) = (2*ll*(ll-1)/klb^2 - clt^2)*klb*spherical_bessel_j(ll,klb) + 4*spherical_bessel_j(ll+1,klb);
%
A(1,2) = (2*ll*(ll-1)/kla^2 - clt^2)*kla*spherical_bessel_y(ll,kla) + 4*spherical_bessel_y(ll+1,kla);
A(2,2) = (2*ll*(ll-1)/klb^2 - clt^2)*klb*spherical_bessel_y(ll,klb) + 4*spherical_bessel_y(ll+1,klb);
%
A(1,3) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_j(ll,kta)./kta - spherical_bessel_j(ll+1,kta));
A(2,3) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_j(ll,ktb)./ktb - spherical_bessel_j(ll+1,ktb));
%
A(1,4) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_y(ll,kta)./kta - spherical_bessel_y(ll+1,kta));
A(2,4) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_y(ll,ktb)./ktb - spherical_bessel_y(ll+1,ktb));
%
A(1,5) = spherical_bessel_j(ll,ka) * clt2./c_ratio^2./rho_ratio;
A(2,5) = 0;
% ----------------
A(3,1) = (ll-1)*spherical_bessel_j(ll,kla)./kla - spherical_bessel_j(ll+1,kla);
A(4,1) = (ll-1)*spherical_bessel_j(ll,klb)./klb - spherical_bessel_j(ll+1,klb);
%
A(3,2) = (ll-1)*spherical_bessel_y(ll,kla)./kla - spherical_bessel_y(ll+1,kla);
A(4,2) = (ll-1)*spherical_bessel_y(ll,klb)./klb - spherical_bessel_y(ll+1,klb);
%
A(3,3) = ((ll^2-1)./kta - kta./2)*spherical_bessel_j(ll,kta) + spherical_bessel_j(ll+1,kta);
A(4,3) = ((ll^2-1)./ktb - ktb./2)*spherical_bessel_j(ll,ktb) + spherical_bessel_j(ll+1,ktb);
%
A(3,4) = ((ll^2-1)./kta - kta./2)*spherical_bessel_y(ll,kta) + spherical_bessel_y(ll+1,kta);
A(4,4) = ((ll^2-1)./ktb - ktb./2)*spherical_bessel_y(ll,ktb) + spherical_bessel_y(ll+1,ktb);
%
A(3,5) = 0;
A(4,5) = 0;
% ----------------
A(5,1) = ll*spherical_bessel_j(ll,kla)./kla - spherical_bessel_j(ll+1,kla);
A(5,2) = ll*spherical_bessel_y(ll,kla)./kla - spherical_bessel_y(ll+1,kla);
A(5,3) = ll*(ll+1)*spherical_bessel_j(ll,kta)./kta;
A(5,4) = ll*(ll+1)*spherical_bessel_y(ll,kta)./kta;
A(5,5) = -d_spherical_bessel_j(ll,ka)./ka;
% -----------------------------------------------------------------------------------