% ------------------------------
function D = D_vector(ll,kla,nu)
% ------------------------------
% returns the D vector defined by Mehl (1985) equation (20)
% providing the boundary condition at the interface between the inner gas and the elastic shell
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   kla : k_l*a variable
%   nu : Poisson coefficient of the shell
%
% outputs
%   D : D (1x4) vector
%
clt = sqrt(2*(1-nu)./(1-2*nu)); % V_P/V_S (or k_S/k_P) ratio in the shell
kta = clt*kla; % k_t*a
%
D = zeros(1,4); % D is a 1x4 vector
%
D(1) = ll*spherical_bessel_j(ll,kla)./kla - spherical_bessel_j(ll+1,kla);
D(2) = ll*spherical_bessel_y(ll,kla)./kla - spherical_bessel_y(ll+1,kla);
D(3) = ll*(ll+1)*spherical_bessel_j(ll,kta)./kta;
D(4) = ll*(ll+1)*spherical_bessel_y(ll,kta)./kta;
