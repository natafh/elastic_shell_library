% ---------------------------------------------------------
function [S_n, ill_A, A_rcond] = S_n_admittance_factor(ll,kla,nu,bar,c_ratio)
% ---------------------------------------------------------
% returns the admittance factor S_n defined by Mehl (1985) equation (22)
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   kla : k_l*a variable
%   nu : Poisson coefficient of the shell
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%   c_ratio : V_P/csound velocity ratio
%
% outputs
%   S_n : dimensionless admittance factor (function of k_l*a)
%   ill_A : true if matrix A is ill-conditioned
%   A_rcond : rcond of the A matrix
%
clt2 = 2*(1-nu)./(1-2*nu); % (V_P/V_S)^2 ratio in the shell
ka = c_ratio*kla; % k*a in the gas
%
C_norm = C_vector_norm(ll,ka);
D = D_vector(ll,kla,nu);
A = A_matrix(ll,kla,nu,bar);
% ill_A true if matrix A is ill-conditioned
A_rcond = rcond(A); % condition number of the A matrix
if (A_rcond < eps)
    ill_A = true;
else
    ill_A = false;
end
%
warning('off','MATLAB:nearlySingularMatrix'); % do not write warning
B_norm = A\C_norm; % matrix left division
%
%
S_n = clt2.*D*B_norm./C_norm(1); % matrix product : (1x4) D times (4x1) B_norm
