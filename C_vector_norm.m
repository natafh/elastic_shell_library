% ------------------------------------
function C_norm = C_vector_norm(ll,ka)
% ------------------------------------
% returns the C vector defined by Mehl (1985) below equation (17),
% normalized by the 'constant' term -a p_0/(rho_s V_S^2)
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   ka : k*a variable (k = radial wave number in the gas)
%
% outputs
%   C_norm : C_norm (4x1) vector
%
C_norm = zeros(4,1); % C is a 4x1 vector
%
C_norm(1) = spherical_bessel_j(ll,ka);
% other elements are zero