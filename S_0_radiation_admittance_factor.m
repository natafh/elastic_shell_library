% --------------------------------------------------------------------------
function S_0 = S_0_radiation_admittance_factor(kla,nu,bar,c_ratio,rho_ratio)
% --------------------------------------------------------------------------
% returns the l=0 admittance factor S_0 defined by Moldover et al (1986),
% equations (49-52), which includes the effect of radiation.
%
% The gas outside the shell can be different from the gas inside it.
% The ratios are of the shell over the outer gas properties.
%
% Because of radiation, S_0 has an imaginary part.
%
% 190704 HCN
%
% inputs
%   kla : k_l*a variable (A in the article)
%   nu : Poisson coefficient of the shell
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%   c_ratio : V_P/c_out velocity ratio (c_out : sound velocity in outer gas)
%   rho_ratio : shell density/outer gas density (enter Inf to kill radiation)
%
% outputs
%   S_0 : l=0 dimensionless admittance factor (function of k_l*a)
%
% clt2 = 2*(1-nu)./(1-2*nu); % (V_P/V_S)^2 ratio in the shell
qq = 0.5*(1-nu)./(1-2*nu); % (q in the article)
klb = bar*kla; % k_l*b (B in the article)
kob = c_ratio*klb; % k'*b in the outer gas
%
RR = kob^2/(1+i*kob)./(rho_ratio*c_ratio^2); % complex radiation factor : R in the article
%
G1 = (1+kla*klb-qq*klb^2)*tan(klb-kla) - (klb-kla)-qq*kla*klb^2; % G_1 in the article
G2 = (1+kla*klb)*tan(klb-kla) - (klb-kla); % G_2 in the article (radiation term)
G3 = ((qq*kla^2-1)*(qq*klb^2-1)+kla*klb)*tan(klb-kla) - (1+qq*kla*klb)*(klb-kla); % G_3
G4 = (1+kla*klb-qq*kla^2)*tan(klb-kla) - (klb-kla)+qq*kla^2*klb; % G_4 (radiation term)
%
S_0 = -qq*(G1 + qq*RR*G2)/(G3 + qq*RR*G4); % l=0 dimensionless admittance factor S_0
%
% to get the relative frequency shif on the acoustic modes, use : 
% delta_f_ratio_rad(nnn) = S_0./(rho_i_ratio*c_i_ratio^2);
% where the _i_ratios are shell/inner gas ratios
% the real part of delta_f_ratio_rad gives the frequency shift
% its imaginary part gives the half-width attenuation
