% test shell+fluid modes for DTS-sodium with Lonzaga et al (2011) formulation
% ************* cannot be done yet : no inner sphere ! ***************
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'compute_modes'])
addpath([acoustic_dir 'compute_modes/drawing_functions'])
%
% mode numbers
l_max = 18;
n_max = 7;
%
%% sodium properties
% ---------------
temperature = 125.; % (Celsius) sodium temperatur
csound = 2500.; % sound velocity in sodium (m/s)
rho_f = 980. ; % sodium density (kg/m^3)
%
%% DTS elastic shell parameters
% -------------------------------
rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
rf(2) = 0.210; % internal radius plexi sphere (m)
hat(2) = 0.005 ; % shell thickness (m)
rho(2) = 7990.; % shell density (kg/m^3)
nu(2) = 0.3 ; % shell Poisson's ratio
mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
%
% not used :
hat(1) = 0.; rho(1) = 0.; mu(1) = 0; nu(1) = 0.;
%
%% compute and plot the elasto-acoustic modes
% -------------------------------------------
[kk, ff, V_P] = elasto_acoustic_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max);
%
full_name = 'test elasto-acoustic modes for ZoRo2';
h_fig = figure('name','elasto-acoustic mode frequencies','position',[100 100 600 800]);
draw_mode_frequencies(h_fig,n_max,l_max,ff,csound,full_name);
%
%save('temporary_ZoRo2_shell_modes')
%