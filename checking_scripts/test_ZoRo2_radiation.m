% test radiation effect for l=0 modes in ZoRo2
% following Moldover et al (1986).
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'compute_modes'])
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'compute_modes/drawing_functions'])
%addpath([acoustic_dir 'utilitaires'])
%
%% ZoRo2 elastic shell parameters
% -------------------------------
rf(1) = 0; % no inner sphere (m)
r_pol = 0.190; % (m) polar radius (plan Max 13/10/2017)
r_equ = 0.200; % (m) equatorial radius (plan Max 13/10/2017)
[r_o, ellipticity] = get_ellipticity(r_equ, r_pol, 'Mehl');
rf(2) = r_o;
hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
%
V_P = sqrt(mu(2)./rho(2) .* 2*(1-nu(2))./(1-2*nu(2))); % P-wave velocity in the shell
%
%% load acoustic modes of ZoRo2 with a rigid shell
% -------------------------------------------------
%filename = 'ZoRo2-air-rigid';
filename = 'ZoRo2-SF6-rigid';
[parameters,n_max,l_max, kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, ReadMe_elastic_modes] = read_acoustic_modes(filename);
[csound, rho_f] = gas_properties('SF6', 20, 1e6); % 20 C, ~10 bar
%
%% compute the relative frequency shift due to the elastic shell
% --------------------------------------------------------------
bar = 1 + hat(2)./rf(2);
%csound = parameters.csound;
c_i_ratio = V_P./csound; % gas inside the shell
%rho_f = parameters.rho_f;
rho_i_ratio = rho(2)./rho_f; % gas inside the shell
%
if (0==0)
% radiate into ambiant air
    [csound_air, rho_f_air] = gas_properties('air', 20, 1e5); % 20 C, ~1 bar
    c_o_ratio = V_P./csound_air; % ratio for gas outside the shell
    rho_o_ratio = rho(2)./rho_f_air; % ratio for gas outside the shell
%
else
    c_o_ratio = c_i_ratio;
    rho_o_ratio = rho_i_ratio;
end
%
ll=0;
lll=ll+1;
full_name = 'test radiation for ZoRo2';
%
% with radiation
% --------------
for nn=0:n_max
    nnn=nn+1;
    ka = kk(nnn,lll);
    kla = ka./c_i_ratio; % k_l*a in the shell
    S_0 = S_0_radiation_admittance_factor(kla,nu(2),bar,c_o_ratio,rho_o_ratio)
    delta_f_ratio_rad(nnn) = S_0./(rho_i_ratio*c_i_ratio^2);
end
%
% without radiation
% -----------------
rho_o_ratio = Inf; % (no outer gas => no radiation)
%
for nn=0:n_max
    nnn=nn+1;
    ka = kk(nnn,lll);
    kla = ka./c_i_ratio; % k_l*a in the shell
    S_0 = S_0_radiation_admittance_factor(kla,nu(2),bar,c_o_ratio,rho_o_ratio)
    delta_f_ratio_norad(nnn) = S_0./(rho_i_ratio*c_i_ratio^2);
% compare with general S_n formula
    delta_f_ratio_ori(nnn) = approximate_shell_deltaf(ll,ka,nu(2),bar,c_i_ratio, rho_i_ratio);
end
%
h_fig = figure('name','acoustic mode relative shift','position',[100 100 600 800]);
plot((0:n_max),real(delta_f_ratio_rad),'+r','DisplayName','radiation')
%draw_elastic_modes(h_fig,n_max,0,real(delta_f_ratio_rad),csound,full_name);
hold on
plot((0:n_max),delta_f_ratio_norad,'ob','DisplayName','no radiation')
plot((0:n_max),delta_f_ratio_ori,'xm','DisplayName','general S_n')
xlabel('radial mode number n')
ylabel('frequency shift ratio')
title(['relative frequency shift \deltaf/f for ZoRo2 with SF6 at 10 bar' char(10) 'l=0 with and without radiation - ' date])
legend('show')
%draw_elastic_modes(h_fig,n_max,0,delta_f_ratio_norad,csound,full_name);
%
h_fig = figure('name','imaginary part','position',[100 100 600 800]);
%draw_elastic_modes(h_fig,n_max,0,imag(delta_f_ratio_rad),csound,full_name);
plot((0:n_max),imag(delta_f_ratio_rad),'*r')
xlabel('radial mode number n')
ylabel('relative width g/f')
title(['radiative loss g/f for ZoRo2 with SF6 at 10 bar' char(10) 'l=0 with radiation - ' date])
%
h_fig = figure('name','radiation relative shift difference','position',[100 100 600 800]);
%draw_elastic_modes(h_fig,n_max,0,real(delta_f_ratio_rad)-delta_f_ratio_norad, csound,full_name);
plot((0:n_max),real(delta_f_ratio_rad)-delta_f_ratio_norad,'*m')
xlabel('radial mode number n')
ylabel('frequency shift ratio difference')
title(['relative frequency shift \deltaf/f|_{rad} - \deltaf/f|_{norad} for ZoRo2 with SF6 at 10 bar' char(10) 'l=0 with and without radiation - ' date])
%
