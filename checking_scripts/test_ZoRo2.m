% test shell modes for ZoRo2
%
coul = 'kbrmgkbrmgkbrmgkbrmg';
marker = 'ox+vsphox+vspho';
%
[acoustic_dir, octave] = get_acoustic_dir;
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'compute_modes'])
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'compute_modes/drawing_functions'])
%addpath([acoustic_dir 'utilitaires'])
% mode numbers
l_max = 18;
n_max = 2;
%
%% ZoRo2 elastic shell parameters
% -------------------------------
rf(1) = 0; % no inner sphere (m)
r_pol = 0.190; % (m) polar radius (plan Max 13/10/2017)
r_equ = 0.200; % (m) equatorial radius (plan Max 13/10/2017)
[r_o, ellipticity] = get_ellipticity(r_equ, r_pol, 'Mehl');
rf(2) = r_o;
hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
%
%% compute and plot the modes of the elastic shell
% ------------------------------------------------
[kk_shell, ff_shell, V_P] = elastic_shell_modes(rf, hat, mu, nu, rho, n_max, l_max);
%
full_name = 'test shell modes for ZoRo2';
h_fig = figure('name','shell mode frequencies','position',[100 100 600 800]);
draw_shell_modes(h_fig,n_max,l_max,ff_shell,V_P,full_name);
%
%save('temporary_ZoRo2_shell_modes')
%
%% load acoustic modes of ZoRo2 with a rigid shell
% -------------------------------------------------
filename = 'ZoRo2-air-rigid';
[parameters,n_max,l_max_ac,kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, ReadMe_elastic_modes] = read_acoustic_modes(filename);
%
%% compute the relative frequency shift due to the elastic shell
% --------------------------------------------------------------
bar = 1 + hat(2)./rf(2);
csound = parameters.csound;
c_ratio = V_P./csound;
rho_f = parameters.rho_f;
rho_ratio = rho(2)./rho_f;
%
for nn=0:n_max
    nnn=nn+1;
    for ll=1:l_max_ac
        lll=ll+1;
        ka = kk(nnn,lll);
        [delta_f_ratio(nnn,lll), ill_A, A_rcond] = approximate_shell_deltaf(ll,ka,nu(2),bar,c_ratio,rho_ratio);
        if ill_A
            fprintf(['   *** ill-conditioned A matrix for n = %i, l = %i : rcond = %g' char(10)],nn,ll,A_rcond)
        end
    end
end
h_fig = figure('name','acoustic mode relative shift','position',[100 100 600 800]);
for nn=n_max:-1:0
	nnn=nn+1;
	to_legend = int2str(nn);
	if nn==n_max
	   to_legend = ['n=' to_legend];
	end
	plot((1:l_max+1)-1, delta_f_ratio(nnn,1:l_max+1),[coul(nnn) marker(nnn)],'MarkerFaceColor', coul(nnn),'DisplayName',to_legend)
    hold on
end
xlabel('angular mode number l')
ylabel('frequency shift ratio')
title(['relative frequency shift \deltaf/f for ZoRo2' char(10) 'with air at 1 bar - ' date])
legend('show')
grid
