% small script to control the behaviour of the determinant of A
% whose zeros yield the shell mode frequencies (see Mehl, 1985)
% -------------------------------------------------------------
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'acoustic_library'])
%
%% Mehl's elastic shell parameters for aluminum
% --------------------------------------------
rf = 0.05; % (m) a value
rho = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
mu = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
nu = 0.355 ; % shell Poisson's ratio for aluminum in Mehl (1985)
%
f_norm = 20.4e3; % (Hz) normalizing frequency
%
bar = 1.05; % b/a ratio for this test
%
%% evolution of det(A) vs kla for various l
% -----------------------------------------
%
kla = (0:0.01:2);
%
for ll=0:4
    for ii=1:length(kla)
        det_A(ii) = det(A_matrix(ll,kla(ii),nu,bar));     
    end
%
    figure('Name',['ll = ' int2str(ll)])
    plot(kla,det_A)
    ylim([-0.1 0.1])
    hold on
    plot(xlim,[0 0],'k:')
end