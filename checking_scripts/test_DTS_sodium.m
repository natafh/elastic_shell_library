% test shell modes for DTS-sodium
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'compute_modes'])
addpath([acoustic_dir 'compute_modes/drawing_functions'])
% mode numbers
l_max = 18;
n_max = 2;
%
%% DTS elastic shell parameters
% -------------------------------
rf(1) = 0.074; % radius of the inner sphere (m) (set to 0 if no inner sphere)
rf(2) = 0.210; % internal radius plexi sphere (m)
hat(2) = 0.005 ; % shell thickness (m)
rho(2) = 7990.; % shell density (kg/m^3)
nu(2) = 0.3 ; % shell Poisson's ratio
mu(2) = 1.8e11/2/(1+nu(2)); % shell shear modulus (Pa)
%
%% compute and plot the modes of the elastic shell
% ------------------------------------------------
[kk_shell, ff_shell, V_P] = elastic_shell_modes(rf, hat, mu, nu, rho, n_max, l_max);
%
full_name = 'test shell modes for DTS-sodium';
h_fig = figure('name','shell mode frequencies','position',[100 100 600 800]);
draw_shell_modes(h_fig,n_max,l_max,ff_shell,V_P,full_name);
%%
%% load acoustic modes of DTS-sodium with a rigid shell
% -------------------------------------------------
filename = 'DTS-sodium-rigid';
[parameters,n_max,l_max_ac, kk,ff,BB,C_nl,Acc,gamma_nl,dk2_nlm,g_nl,delta_f_nl, ReadMe_elastic_modes] = read_acoustic_modes(filename);
%
%% compute the relative frequency shift due to the elastic shell
% --------------------------------------------------------------
bar = 1 + hat(2)./rf(2);
csound = parameters.csound;
c_ratio = V_P./csound;
rho_f = parameters.rho_f;
rho_ratio = rho(2)./rho_f;
%
for nn=0:n_max
    nnn=nn+1;
    for ll=1:l_max_ac
        lll=ll+1;
        ka = kk(nnn,lll);
        delta_f_ratio(nnn,lll) = approximate_shell_deltaf(ll,ka,nu(2),bar,c_ratio, rho_ratio);
    end
end
h_fig = figure('name','acoustic mode relative shift','position',[100 100 600 800]);
draw_mode_frequencies(h_fig,n_max,l_max_ac,delta_f_ratio,csound,full_name);
