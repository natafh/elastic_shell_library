% test shell+fluid modes for ZoRo2 with Lonzaga et al (2011) formulation
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'acoustic_library'])
addpath([acoustic_dir 'ellipticity_library'])
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'compute_modes/drawing_functions'])
% mode numbers
l_max = 18;
n_max = 7;
%
%% air properties
% ---------------
gas_name = 'air';
temperature = 20.; % (Celsius) gas temperature
pressure = 1.013e5; % (Pa) gas pressure
[csound, rho_f] = gas_properties(gas_name, temperature, pressure);
%
%% ZoRo2 elastic shell parameters
% -------------------------------
rf(1) = 0; % no inner sphere (m)
r_pol = 0.190; % (m) polar radius (plan Max 13/10/2017)
r_equ = 0.200; % (m) equatorial radius (plan Max 13/10/2017)
[r_o, ellipticity] = get_ellipticity(r_equ, r_pol, 'Mehl');
rf(2) = r_o;
hat(2) = 0.01 ; % (m) shell thickness (plan Max 13/10/2017)
rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
nu(2) = 0.33 ; % shell Poisson's ratio Alu 7075 data sheet
%
% not used :
hat(1) = 0.; rho(1) = 0.; mu(1) = 0; nu(1) = 0.;
%
%% compute and plot the elasto-acoustic modes
% -------------------------------------------
[kk, ff, V_P] = elasto_acoustic_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max);
%
full_name = 'test elasto-acoustic modes for ZoRo2';
h_fig = figure('name','elasto-acoustic mode frequencies','position',[100 100 600 800]);
draw_mode_frequencies(h_fig,n_max,l_max,ff,csound,full_name);
%
%save('temporary_ZoRo2_shell_modes')
%