% small script to reproduce Figure 1 of Mehl (1985)
% -------------------------------------------------
%
couleur = 'brkgcmybrkgcmybrkgcmybrkgcmybrkgcmy';
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'acoustic_library'])
%
% default values
rf(1) = 0.; % no inner sphere
mu(1) = Inf; % rigid inner sphere
hat(1) = Inf; % dumy
rho(1) = 1.; % dumy
nu(1) = 1.; % dumy
%
% mode numbers
l_max = 6;
% l_max = 6;
n_max = 1;
%
%% Mehl's elastic shell parameters for aluminum
% --------------------------------------------
rf(2) = 0.05; % (m) a value
rho(2) = 2810.; % shell density (kg/m^3) Alu 7075 data sheet
mu(2) = 26.9e9; % (Pa) shell shear modulus Alu 7075 data sheet
nu(2) = 0.355 ; % shell Poisson's ratio for aluminum in Mehl (1985)
%
f_norm = 20.4e3; % (Hz) normalizing frequency
%
%b_to_a_ratio = (1:0.05:2); % b/a ratio varied between 1+ and 2
b_to_a_ratio = (1:0.02:2); % b/a ratio varied between 1+ and 1.2
b_to_a_ratio(1) = []; % suppress b/a=1 (ie no shell)
n_bar = length(b_to_a_ratio);
%
%% find modes
% -----------
for ii=1:n_bar
    disp([' ---------- b/a ratio = ' num2str(b_to_a_ratio(ii)) ' ---------------'])
    hat(2) = rf(2)*(b_to_a_ratio(ii) - 1) ; % (m) shell thickness from b/a ratio
    [kk_shell, ff_shell, V_P] = elastic_shell_modes(rf, hat, mu, nu, rho, n_max, l_max);
    Lambda_ns(ii,:) = ff_shell(1,:)/f_norm;
end
%
full_name = 'compare with Figure 1 of Mehl (1985)';
h_fig = figure('name','shell mode frequencies','position',[100 100 600 800]);
hold on
%
%% plot
% -----
for ll=0:l_max
    lll = ll+1;
    to_plot(1:n_bar) = Lambda_ns(:,lll); 
    plot(b_to_a_ratio,to_plot,'*-','color',[couleur(lll)], 'DisplayName',int2str(ll))
end
title([full_name ' - ' date])
xlabel('b/a')
ylabel('normalized \Lambda_{ns}')
box('on')
legend('show')