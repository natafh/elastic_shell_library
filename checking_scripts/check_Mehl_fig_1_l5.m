% small script to check rise of Lambda_ns with b/a in Figure 1 of Mehl (1985)
% ---------------------------------------------------------------------------
%
couleur = 'brkgcmybrkgcmybrkgcmybrkgcmybrkgcmybrkgcmybrkgcmybrkgcmybrkgcmybrkgcmy';
%
[acoustic_dir, octave] = get_acoustic_dir;
%
% add paths for functions used in this program
addpath([acoustic_dir 'elastic_shell_library'])
addpath([acoustic_dir 'acoustic_library'])
%
%% Mehl's elastic shell parameters for aluminum
% --------------------------------------------
nu = 0.355 ; % shell Poisson's ratio for aluminum in Mehl (1985)
%
b_to_a_ratio = (1:0.02:2); % b/a ratio varied between 1+ and 1.2
b_to_a_ratio(1) = []; % suppress b/a=1 (ie no shell)
n_bar = length(b_to_a_ratio);
%
%% compute det(A)
% ---------------
kla = (0:0.005:1.5);
kla(1) = [];
ll = 5; % test l=5, which shows a large rise with b/a
%
h_fig = figure('name','shell mode frequencies','position',[100 100 600 800]);
hold on
%
for ii=1:n_bar
    disp([' ---------- b/a ratio = ' num2str(b_to_a_ratio(ii)) ' ---------------']) 
    for kk=1:length(kla)
        det_A(kk) = det(A_matrix(ll,kla(kk),nu, b_to_a_ratio(ii)));
    end    
    plot(kla,det_A,'.-','color',[couleur(ii)], 'DisplayName',['b/a= ' num2str(b_to_a_ratio(ii))])
end
%
title([' test rise of \Lambda_{ns} with b/a for l = ' int2str(ll) ' - ' date])
xlabel('kla')
ylabel('det(A)')
box('on')
legend('show')