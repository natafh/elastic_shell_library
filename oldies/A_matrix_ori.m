% -------------------------------------------
function A = A_matrix(ll,kla,nu,bar)
% -------------------------------------------
% returns the matrix A defined by Mehl (1985) equation (18)
% controlling modes in a spherical elastic shell extending from r=a to r=b
% with Poisson coefficient nu, assuming free boundary condition at r=b (no radiation).
% Resonance occurs when det(A) = 0.
% Subscripts _l and _t stand for longitudinal (P) and transversal (S), respectively
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   kla : k_l*a variable
%   nu : Poisson coefficient of the shell
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%
% outputs
%   A : A (4x4) matrix
%
    clt = sqrt(2*(1-nu)./(1-2*nu)); % V_P/V_S (or k_S/k_P) ratio in the shell
    klb = bar*kla; % k_l*b
    kta = clt*kla; % k_t*a
    ktb = bar*kta; % k_t*b
%
    A = zeros(4,4); % A is a 4x4 matrix
%
    A(1,1) = (2*ll*(ll-1)/kla^2 - clt^2)*spherical_bessel_j(ll,kla) + 4*spherical_bessel_j(ll+1,kla);
    A(2,1) = (2*ll*(ll-1)/klb^2 - clt^2)*spherical_bessel_j(ll,klb) + 4*spherical_bessel_j(ll+1,klb);
%
    A(1,2) = (2*ll*(ll-1)/kla^2 - clt^2)*spherical_bessel_y(ll,kla) + 4*spherical_bessel_y(ll+1,kla);
    A(2,2) = (2*ll*(ll-1)/klb^2 - clt^2)*spherical_bessel_y(ll,klb) + 4*spherical_bessel_y(ll+1,klb);
%
    A(1,3) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_j(ll,kta)./kta - spherical_bessel_j(ll+1,kta));
    A(2,3) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_j(ll,ktb)./ktb - spherical_bessel_j(ll+1,ktb));
%
    A(1,4) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_y(ll,kta)./kta - spherical_bessel_y(ll+1,kta));
    A(2,4) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_y(ll,ktb)./ktb - spherical_bessel_y(ll+1,ktb));
%
    A(3,1) = (ll-1)*spherical_bessel_j(ll,kla)./kla - spherical_bessel_j(ll+1,kla);
    A(4,1) = (ll-1)*spherical_bessel_j(ll,klb)./klb - spherical_bessel_j(ll+1,klb);
%
    A(3,2) = (ll-1)*spherical_bessel_y(ll,kla)./kla - spherical_bessel_y(ll+1,kla);
    A(4,2) = (ll-1)*spherical_bessel_y(ll,klb)./klb - spherical_bessel_y(ll+1,klb);
%
    A(3,3) = ((ll^2-1)./kta - kta./2)*spherical_bessel_j(ll,kta) + spherical_bessel_j(ll+1,kta);
    A(4,3) = ((ll^2-1)./ktb - ktb./2)*spherical_bessel_j(ll,ktb) + spherical_bessel_j(ll+1,ktb);
%
    A(3,4) = ((ll^2-1)./kta - kta./2)*spherical_bessel_y(ll,kta) + spherical_bessel_y(ll+1,kta);
    A(4,4) = ((ll^2-1)./ktb - ktb./2)*spherical_bessel_y(ll,ktb) + spherical_bessel_y(ll+1,ktb);
%
end