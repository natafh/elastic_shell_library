% ------------------------------------------------------------------------------------------
function [kk_shell, ff_shell, V_P] = elastic_shell_modes(rf, hat, mu, nu, rho, n_max, l_max)
% ------------------------------------------------------------------------------------------
% computes the 'k_l*a' kk_shell and frequency ff_shell of elastic modes (nn,ll)
% in a spherical elastic shell extending from r=a to r=b.
% Does not use the thin shell approximation.
% Seismological notation used (ex: 0S13, 1S0, etc)
% note : ff_shell = kk_shell * V_P/(2 pi r_o)
% 190625 HCN : adapted from paper by Mehl (1985), following Aurélien modes_sphere matlab program
% the inputs are the same 2-elements properties (1=inner sphere, 2=outer shell) as
% given in compute_modes_menu but only the outer shell is considered here
% 
% inputs :
%	rf(1) = radius of the inner sphere (m) -- not used
%	rf(2) = radius of the outer sphere (m)
%	hat(1) = thickness of the inner elastic shell (m) -- not used
%	hat(2) = thickness of the outer elastic shell (m)
%	mu(1) = shear modulus of the inner elastic shell (Pa) (Inf if rigid) -- not used
%	mu(2) = shear modulus of the outer elastic shell (Pa) (Inf if rigid)
%	nu(1) = Poisson's ratio of the inner elastic shell -- not used
%	nu(2) = Poisson's ratio of the outer elastic shell
%	rho(1) = density of the inner elastic shell (kgm^-3) -- not used
%	rho(2) = density of the outer elastic shell (kgm^-3)
%	n_max = max radial mode number (0 to n_max)
%	l_max = max angular mode number (0 to l_max)
%
% outputs :
%	kk_shell(nn+1,ll+1) = 'k_l*a' of mode nnS_ll
%	ff_shell(nn+1,ll+1) = frequency of mode nnS_ll (Hz)
%   V_P = P-wave velocity in the shell (m/s)
%
	disp(['Welcome in elastic_shell_modes,' char(10) 'which computes the elastic modes of a spherical shell.' char(10)])	    

% modes of the elastic shell
	f_shell = @(l,x) det(A_matrix(l,x,nu(2),1+hat(2)./rf(2)));
	kk_shell = f_modes(f_shell, n_max, l_max); % 'k_l*a' of modes (0:n_max,0:l_max)
	V_P = sqrt(mu(2)./rho(2) .* 2*(1-nu(2))./(1-2*nu(2))); % P-wave velocity in the shell
    ff_shell = kk_shell*V_P/(2*pi*rf(2)); % frequency of modes (nn,ll)
end
% ---------------------------------
function z = f_modes(f, nmax, lmax)
% ---------------------------------
	z = zeros(nmax+1,lmax+1);	
	for l = 0:lmax
		z(:,l+1) = fzeros(@(x) f(l,x), (l+1)./10, Inf, nmax+1, 1); % *** affiner choix x0 ***
	end	    
% correction pour le mode (0,0) *** a voir ***
%	z(:,1)=[0; z(1:end-1,1)];
end
% -------------------------------------------
function z = fzeros(f, x0, xmax, nmax, delta)
% -------------------------------------------
%% fzeros : résolution de f(x) = 0
% f = fonction dont on cherche les zéros
% x0 = point de départ (début intervalle : doit être inférieur à tous les
% zéros recherchés)
% xmax = fin de l'intervalle de recherche (peut être +Inf)
% nmax = nombre de zéros recherchés (peut être +Inf)
% delta = écart minimal entre les zéros
%
% **********
    octave=0;
% **********
	threshold = 1;   % limite déterminant si deux valeurs trouvées sont égales
	alloc_step = 128;   % si le nombre de zéros n'est pas spécifié, on alloue un tableau par tranches arbitraires
    
	if nmax < Inf
		z = zeros(nmax,1);
	else
		z = zeros(alloc_step,1);
	end

	n = 1;
	x = x0;
	while x < xmax && n <= nmax
% recherche le zéro le plus proche
        if octave
            [xn, ~, err] = fsolve(f, x); % 130829 HCN : replace fzero by fsolve
        else
    		[xn, ~, err] = fzero(f, x, optimset('Display','off'));
%    		[xn, ~, err] = fzero(f, x, optimset('PlotFcns',{@optimplotx,@optimplotfval}));
    		
        end
% vérifie si on a trouvé une nouvelle valeur
	if err == 1 && (n == 1 || abs(xn - z(n - 1)) > threshold)
		z(n) = xn;
		n = n + 1;
% augmente la taille du tableau si nécessaire
			if nmax == Inf && mod(n, alloc_step) == 0
				z = [z ; zeros(alloc_step, 1)];
			end
			x = xn;
		end
		x = x + delta;
	end
	z(n:end) = [];
end

%% ================== fonction for elastic shell =================== %
% -------------------------------------------
function A = A_matrix(ll,kla,nu,bar)
% -------------------------------------------
% returns the matrix A defined by Mehl (1985) equation (18)
% controlling modes in a spherical elastic shell extending from r=a to r=b
% with Poisson coefficient nu, assuming free boundary condition at r=b (no radiation).
% Resonance occurs when det(A) = 0.
% Subscripts _l and _t stand for longitudinal (P) and transversal (S), respectively
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   kla : k_l*a variable
%   nu : Poisson coefficient of the shell
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%
% outputs
%   A : A (4x4) matrix
%
    clt = sqrt(2*(1-nu)./(1-2*nu)); % V_P/V_S (or k_S/k_P) ratio in the shell
    klb = bar*kla; % k_l*b
    kta = clt*kla; % k_t*a
    ktb = bar*kta; % k_t*b
%
    A = zeros(4,4); % A is a 4x4 matrix
%
    A(1,1) = (2*ll*(ll-1)/kla^2 - clt^2)*spherical_bessel_j(ll,kla) + 4*spherical_bessel_j(ll+1,kla);
    A(2,1) = (2*ll*(ll-1)/klb^2 - clt^2)*spherical_bessel_j(ll,klb) + 4*spherical_bessel_j(ll+1,klb);
%
    A(1,2) = (2*ll*(ll-1)/kla^2 - clt^2)*spherical_bessel_y(ll,kla) + 4*spherical_bessel_y(ll+1,kla);
    A(2,2) = (2*ll*(ll-1)/klb^2 - clt^2)*spherical_bessel_y(ll,klb) + 4*spherical_bessel_y(ll+1,klb);
%
    A(1,3) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_j(ll,kta)./kta - spherical_bessel_j(ll+1,kta));
    A(2,3) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_j(ll,ktb)./ktb - spherical_bessel_j(ll+1,ktb));
%
    A(1,4) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_y(ll,kta)./kta - spherical_bessel_y(ll+1,kta));
    A(2,4) = 2*ll*(ll+1) * ((ll-1)*spherical_bessel_y(ll,ktb)./ktb - spherical_bessel_y(ll+1,ktb));
%
    A(3,1) = (ll-1)*spherical_bessel_j(ll,kla)./kla - spherical_bessel_j(ll+1,kla);
    A(4,1) = (ll-1)*spherical_bessel_j(ll,klb)./klb - spherical_bessel_j(ll+1,klb);
%
    A(3,2) = (ll-1)*spherical_bessel_y(ll,kla)./kla - spherical_bessel_y(ll+1,kla);
    A(4,2) = (ll-1)*spherical_bessel_y(ll,klb)./klb - spherical_bessel_y(ll+1,klb);
%
    A(3,3) = ((ll^2-1)./kta - kta./2)*spherical_bessel_j(ll,kta) + spherical_bessel_j(ll+1,kta);
    A(4,3) = ((ll^2-1)./ktb - ktb./2)*spherical_bessel_j(ll,ktb) + spherical_bessel_j(ll+1,ktb);
%
    A(3,4) = ((ll^2-1)./kta - kta./2)*spherical_bessel_y(ll,kta) + spherical_bessel_y(ll+1,kta);
    A(4,4) = ((ll^2-1)./ktb - ktb./2)*spherical_bessel_y(ll,ktb) + spherical_bessel_y(ll+1,ktb);
%
end
% ------------------------------
function D = D_vector(ll,kla,nu)
% ------------------------------
% returns the D vector defined by Mehl (1985) equation (20)
% providing the boundary condition at the interface between the inner gas and the elastic shell
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   kla : k_l*a variable
%   nu : Poisson coefficient of the shell
%
% outputs
%   D : D (1x4) vector
%
    clt = sqrt(2*(1-nu)./(1-2*nu)); % V_P/V_S (or k_S/k_P) ratio in the shell
    kta = clt*kla; % k_t*a
%
    D = zeros(1,4); % D is a 1x4 vector
%
    D(1) = ll*spherical_bessel_j(ll,kla)./kla - spherical_bessel_j(ll+1,kla);
    D(2) = ll*spherical_bessel_y(ll,kla)./kla - spherical_bessel_y(ll+1,kla);
    D(3) = ll*(ll+1)*spherical_bessel_j(ll,kta)./kta;
    D(4) = ll*(ll+1)*spherical_bessel_y(ll,kta)./kta;
%
end
% ------------------------------------
function C_norm = C_vector_norm(ll,ka)
% ------------------------------------
% returns the C vector defined by Mehl (1985) below equation (17),
% normalized by the 'constant' term -a p_0/(rho_s V_S^2)
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   ka : k*a variable (k = radial wave number in the gas)
%
% outputs
%   C_norm : C_norm (1x4) vector
%
    C_norm = zeros(1,4); % C is a 1x4 vector
%
    C_norm(1) = spherical_bessel_j(ll,ka);
% other elements are zero
end
%
% -----------------------------------------------------
function S_n = admittance_factor(ll,kla,nu,bar,c_ratio)
% -----------------------------------------------------
% returns the admittance factor S_n defined by Mehl (1985) equation (22)
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   kla : k_l*a variable
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%   nu : Poisson coefficient of the shell
%   c_ratio : V_P/csound velocity ratio
%
% outputs
%   S_n : dimensionless admittance factor (function of k_l*a)
%
    clt2 = 2*(1-nu)./(1-2*nu); % (V_P/V_S)^2 ratio in the shell
    ka = c_ratio*kla; % k*a in the gas
%
    C_norm = C_vector_norm(ll,ka);
    D = D_vector(ll,kla,nu);
    B_norm = C_norm\A_matrix(ll,kla,nu,bar);
%
    S_n = 0;
    for ii=1:4
        S_n = S_n + B_norm(ii)*D(ii)./C_norm(1);
    end
    S_n = clt2*S_n;
end
% -------------------------------------------------------------------------------
function delta_f_ratio = approximate_shell_deltaf(ll,ka,nu,bar,c_ratio,rho_ratio)
% -------------------------------------------------------------------------------
% returns the frequency correction defined by Mehl (1985) equation (26)
%
% inputs
%   ll : angular degree of the mode (no dependence on azimuthal mode number m)
%   ka : k*a of rigid shell mode (z_ns in Mehl) 
%   bar : b/a ratio (a = shell's inner radius, b = shell's outer radius)
%   nu : Poisson coefficient of the shell
%   rho_ratio : shell density/gas density
%   c_ratio : V_P/csound velocity ratio
%
% outputs
%   delta_f_ratio : delta_f/f correction for acoustic mode of the rigid shell with ka=kk(nn,ll) 
%
    kla = ka./c_ratio; % k_l*a in the shell
    S_n = admittance_factor(ll,kla,nu,bar,c_ratio);
%
    delta_f_ratio = S_n./(1 -ll*(ll+1)./ka^2)./((rho_ratio)*c_ratio^2);
%
end
