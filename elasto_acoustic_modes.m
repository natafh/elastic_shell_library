% -----------------------------------------------------------------------------------------------------------
function [kk, ff, V_P] = elasto_acoustic_modes(csound, rho_f, rf, hat, mu, nu, rho, n_max, l_max)
% -----------------------------------------------------------------------------------------------------------
% computes the 'k*a' kk and frequency ff of elasto-acoustic modes (nn,ll)
% in a spherical elastic shell extending from r=a to r=b.
% Does not use the thin shell approximation.
% Seismological notation used (ex: 0S13, 1S0, etc)
% note : ff = kk * csound/(2 pi r_o)
%
% 190708 HCN *** en cours ***
% adapted from paper by Lonzaga et al (2011),
% following Aurélien modes_sphere matlab program
% the inputs are the same 2-elements properties (1=inner sphere, 2=outer shell) as
% given in compute_modes_menu but only the outer shell is considered here
% 
% inputs :
%	csound = sound velocity in the fluid (m/s)
%	rho_f = density of the fluid (kgm^-3)
%	rf(1) = radius of the inner sphere (m) -- not used
%	rf(2) = radius of the outer sphere (m)
%	hat(1) = thickness of the inner elastic shell (m) -- not used
%	hat(2) = thickness of the outer elastic shell (m)
%	mu(1) = shear modulus of the inner elastic shell (Pa) (Inf if rigid) -- not used
%	mu(2) = shear modulus of the outer elastic shell (Pa) (Inf if rigid)
%	nu(1) = Poisson's ratio of the inner elastic shell -- not used
%	nu(2) = Poisson's ratio of the outer elastic shell
%	rho(1) = density of the inner elastic shell (kgm^-3) -- not used
%	rho(2) = density of the outer elastic shell (kgm^-3)
%	n_max = max radial mode number (0 to n_max)
%	l_max = max angular mode number (0 to l_max)
%
% outputs :
%	kk(nn+1,ll+1) = 'k*a' of mode nnS_ll
%	ff(nn+1,ll+1) = frequency of mode nnS_ll (Hz)
%   V_P = P-wave velocity in the shell (m/s)
%
	disp(['Welcome in elasto_acoustic_modes,' char(10) 'which computes the elasto-acoustic modes' char(10) 'of a fluid-filled elastic spherical shell.' char(10)])	    

% modes of the fluid-filled elastic shell
	V_P = sqrt(mu(2)./rho(2) .* 2*(1-nu(2))./(1-2*nu(2))); % P-wave velocity in the shell
	c_ratio = V_P/csound;
	rho_ratio = rho(2)./rho_f;
	f_elasto = @(l,x) det(A_matrix_Lonzaga(l,x,nu(2),1+hat(2)./rf(2),c_ratio,rho_ratio));
	kk = f_modes(f_elasto, n_max, l_max); % 'k*a' of elasto-acoustic modes (0:n_max,0:l_max)
    ff = kk*csound/(2*pi*rf(2)); % frequency of modes (nn,ll)
end
% ---------------------------------
function z = f_modes(f, nmax, lmax)
% ---------------------------------
	z = zeros(nmax+1,lmax+1);
	x0 = 0.3; % to avoid very large values at small x while getting all zeros
	delta = 1.; % the first two zeros for l>2 are very close to each other
    threshold = 1.;
    for l = 0:lmax
% *********
%       disp([' ================= l = ' int2str(l)])
% *********
        if (l>=2)
% For l>2, the first two zeros are very close to each other
            delta = 0.1;
            threshold = 0.1;
        end
        z(:,l+1) = fzeros(@(x) f(l,x), x0, Inf, nmax+1, delta, threshold);
    end	    
end
% ------------------------------------------------------
function z = fzeros(f, x0, xmax, nmax, delta, threshold)
% ------------------------------------------------------
%% fzeros : résolution de f(x) = 0
% f = fonction dont on cherche les zéros
% x0 = point de départ (début intervalle : doit être inférieur à tous les
% zéros recherchés)
% xmax = fin de l'intervalle de recherche (peut être +Inf)
% nmax = nombre de zéros recherchés (peut être +Inf)
% delta = pas de test des zéros
% threshold = écart minimal entre zeros
%
% **********
    octave=0;
% **********
% For l>2, the first two zeros are very close
%	threshold = 0.01;   % limite déterminant si deux valeurs trouvées sont égales.
	alloc_step = 128;   % si le nombre de zéros n'est pas spécifié, on alloue un tableau par tranches arbitraires
    
	if nmax < Inf
		z = zeros(nmax,1);
	else
		z = zeros(alloc_step,1);
	end

	n = 1;
	x = x0;
	while x < xmax && n <= nmax
% recherche le zéro le plus proche
        if octave
            [xn, ~, err] = fsolve(f, x); % 130829 HCN : replace fzero by fsolve
        else
    		[xn, ~, err] = fzero(f, x, optimset('Display','off'));
%    		[xn, ~, err] = fzero(f, x, optimset('PlotFcns',{@optimplotx,@optimplotfval}));
    		
        end
% vérifie si on a trouvé une nouvelle valeur
	if err == 1 && (n == 1 || abs(xn - z(n - 1)) > threshold)
		z(n) = xn;
		n = n + 1;
% augmente la taille du tableau si nécessaire
			if nmax == Inf && mod(n, alloc_step) == 0
				z = [z ; zeros(alloc_step, 1)];
			end
			x = xn;
		end
		x = x + delta;
	end
	z(n:end) = [];
end